# GitLab Slack Archivist

![](img/gitlab-slack-archivist-icon-128.png)

**Proof of concept, currently not in use except for testing.**

1. Mention @gitlab-slack-archivist in a GitLab comment that it can read.
2. Your comment includes a link to a message in a public channel (or a
   thread in a public channel) in GitLab's Slack.
3. Someone [runs the script][schedule], providing a read-only Slack
   token and the token of the bot user. The bot responds with the message
   contents.

Demo: https://gitlab.com/gitlab-org/gitlab-slack-archivist/issues/5#note_167547546

Ping @gitlab-slack-archivist with a URL to a message in the GitLab Slack
and, if you're a member of the [gitlab-com group], it will reply to your
comment with the text of the message.

Concept stolen from [GifLab]!

## Restrictions

1. The author of the message must be a member of the [gitlab-com group]
   at the time the archivist fetches the message.
2. The Slack message linked to must be in a public channel, not a
   private group.

## Removing messages with sensitive content

Even if a message is in a public Slack channel, that doesn't necessarily
mean it should be archived. The bot's responses are able to be deleted
by anyone who could normally delete comments:

1. Instance admins.
2. Maintainers or Owners of the project or group the comment was posted
   in.
3. The bot user themselves. (See [credentials](#credentials) below.)

To see all messages posted by the bot, log in as the bot user and [view
their profile page][profile].

## Running locally

The tool only takes environment variables, with no command-line options:

1. `GITLAB_API_TOKEN`. This needs to permit access to projects that you
   want to use the archivist in; if you're using it in a private
   project, this token must be able to read that project and comment in
   it.
2. `SLACK_API_TOKEN`. This should have the most limited possible
   permissions (ideally, it can only read public channels).
3. `GITLAB_API_ENDPOINT` (optional; defaults to
   https://gitlab.com/api/v4). Only change this if you want to test with
   another GitLab instance.
3. `GITLAB_GROUP_ID` (optional; defaults to 6543, which is gitlab-com on
   GitLab.com). Only change this if you want to test with another GitLab
   instance or group.

```shell
$ bundle exec ruby archivist.rb
```

## Credentials

The bot user's credentials are in GitLab's default 1Password vault. The
Slack token has [an open issue][slack-token].

## Contributing

Contributions are welcome! See [CONTRIBUTING.md] and the [LICENSE].

[GifLab]: https://gitlab.com/giflab
[gitlab-com group]: https://gitlab.com/gitlab-com
[CONTRIBUTING.md]: CONTRIBUTING.md
[LICENSE]: LICENSE
[schedule]: https://gitlab.com/gitlab-org/gitlab-slack-archivist/issues/1
[slack-token]: https://gitlab.com/gitlab-org/gitlab-slack-archivist/issues/7
[profile]: https://gitlab.com/gitlab-slack-archivist
