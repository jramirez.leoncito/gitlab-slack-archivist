require 'httparty'

class SlackApi
  attr_reader :token

  def initialize(token: nil)
    @token = token
  end

  def get_single_message(method, channel, ts_first, ts_last, latest)
    @get_single_message ||= {}
    @get_single_message[[method, channel, ts_first, ts_last, latest]] ||=
      begin
        message =
          get("https://slack.com/api/#{method}?&channel=#{channel}&ts=#{ts_first}.#{ts_last}&limit=1&latest=#{latest}&inclusive=1").
            fetch('messages').
            first

        message.merge('author_name' => users[message['user']]['real_name'])
      end
  end

  def users
    @users ||= get("https://slack.com/api/users.list").
                 fetch('members').
                 group_by { |user| user['id'] }.
                 map { |k, v| [k, v.first] }.
                 to_h
  end

  private

  def get(url)
    HTTParty.get(url, headers: { 'Authorization' => "Bearer #{token}" })
  end
end
