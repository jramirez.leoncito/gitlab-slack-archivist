require 'slack_api'

RSpec.describe SlackApi, :vcr, :slack do
  # The defaults are used for creating new VCR recordings. Most spec runs
  # won't need them.
  let(:token) { ENV.fetch('SLACK_API_TOKEN', '<SLACK_API_TOKEN>') }
  subject(:api) { described_class.new(token: token) }

  describe '#get_single_message' do
    shared_examples 'getting a single message' do
      subject(:response) { api.get_single_message(*message_args) }

      it 'returns the message from the response' do
        expect(response['text']).to eq(message_text)
      end

      it 'adds the name of the author' do
        expect(response['author_name']).to eq(message_author)
      end

      it 'records the result and does not make further requests for the same arguments' do
        response

        expect(HTTParty).not_to receive(:get)

        api.get_single_message(*message_args)
      end
    end

    context 'with a message in a channel' do
      # https://gitlab.slack.com/archives/C72HPNV97/p1556876971077400
      let(:message_args) do
        ['conversations.history', 'C72HPNV97', '1556876971', '077400', '1556876971.077400']
      end

      let(:message_text) do
        'I added a very early outline of our next group conversation in <https://gitlab.com/gitlab-org/group-conversations/merge_requests/17> :slightly_smiling_face:'
      end

      let(:message_author) { 'Sean McGivern' }

      include_examples 'getting a single message'
    end

    context 'with a message in a thread' do
      # https://gitlab.slack.com/archives/C72HPNV97/p1557233726092300?thread_ts=1557233606.092000&cid=C72HPNV97
      let(:message_args) do
        ['conversations.replies', 'C72HPNV97', '1557233726', '092300', '1557233606.092000']
      end

      let(:message_text) do
        "once we have <https://gitlab.com/gitlab-org/gitlab-ee/issues/9121>, I won't need to say things like 'I'm not sure what the prioritisation' :slightly_smiling_face:"
      end

      let(:message_author) { 'Sean McGivern' }

      include_examples 'getting a single message'
    end
  end

  describe '#users' do
    it 'returns a hash of user IDs to users' do
      expect(api.users).
        to include('U10PLDTPX' => a_hash_including('id' => 'U10PLDTPX', 'real_name' => 'Sean McGivern'))
    end

    it 'records the result and does not make further requests for the same arguments' do
      api.users

      expect(HTTParty).not_to receive(:get)

      api.users
    end
  end
end
